from openai import OpenAI
from config.env import env

assert isinstance(
    env["OPENAI_API_KEY"], str
), "Please set the environment variables CHATGPT API KEY Settings."

client = OpenAI()


class ChatGPT:
    def __init__(self):
        client.api_key = env["OPENAI_API_KEY"]
        self.model = env["CHATGPT_MODEL"]
        self.temperature = env["CHATGPT_TEMPERATURE"]
        self.total_tokens = 0

    def get_response(self, message_list: list):
        response = client.chat.completions.create(
            model=self.model,
            messages=message_list,
        )
        _message = response.choices[0].message.content
        self.total_tokens = response.usage.total_tokens
        return _message
