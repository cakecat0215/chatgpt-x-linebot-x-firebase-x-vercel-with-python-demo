from domain.user import User
from config.env import env

default_ai_guidelines = env["AI_GUIDELINES"]
default_ai_background = env["AI_BACKGROUND"]


class Prompt:

    def __init__(self):
        pass

    @staticmethod
    def add_ai_background_msg(
        user: User,
    ) -> list:
        msg_list = []
        msg_list.append(
            {
                "role": "system",
                "content": (
                    f"{user.ai_background}"
                    if user.ai_background != ""
                    else f"{default_ai_background}"
                ),
            }
        )
        return msg_list

    @staticmethod
    def add_ai_guidelines_msg(user: User) -> list:
        msg_list = []
        msg_list.append(
            {
                "role": "system",
                "content": (
                    f"{user.ai_guidelines}"
                    if user.ai_guidelines != ""
                    else f"{default_ai_guidelines}"
                ),
            }
        )

        return msg_list
