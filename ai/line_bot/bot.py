import asyncio
from flask import request, abort
from linebot.v3 import WebhookHandler
from linebot.v3.webhooks import MessageEvent
from linebot.v3.exceptions import InvalidSignatureError
from .services import LineBotServices
from domain.message import Message
from config.env import env
from event.manager import EventManager


assert isinstance(
    env["LINE_TOKEN"], str
), "Please set the environment variables LINE TOKEN Settings."
assert isinstance(
    env["LINE_SECRET"], str
), "Please set the environment variables LINE SECRET Settings."

handler = WebhookHandler(channel_secret=env["LINE_SECRET"])


class LineBot:

    def line_handler(self):
        # get X-Line-Signature header value
        signature = request.headers["X-Line-Signature"]

        # get request body as text
        body = request.get_data(as_text=True)

        # handle webhook body
        try:
            handler.handle(body, signature)
        except InvalidSignatureError as e:
            print(
                "Error!",
                e,
            )
            abort(400, e)

        return "ok"

    @handler.add(event=MessageEvent)
    def handle_message(event):
        line_bot_services = LineBotServices(
            line_token=env["LINE_TOKEN"]
        )
        line_bot_services.show_loading(user_id=event.source.user_id)
        data = line_bot_services.get_profile(user_id=event.source.user_id).data
        message = Message(
            reply_token=event.reply_token,
            user_id=event.source.user_id,
            message_id=event.message.id,
            type=event.message.type,
            timestamp=event.timestamp,
            display_name=data.display_name,
            language=data.language,
            content=event.message.text if event.message.type == "text" else None,
            quote_token=(
                event.message.quote_token if event.message.type == "text" else ""
            ),
        )

        status, check_message = line_bot_services.check_status(event=event)
        if not status:
            line_bot_services.reply_message(
                reply_token=message.reply_token, text=check_message
            )
            return

        result = asyncio.run(EventManager(line_bot_services=line_bot_services, message=message).start())
