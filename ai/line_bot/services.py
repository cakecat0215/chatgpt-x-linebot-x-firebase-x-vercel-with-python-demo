from linebot.v3.messaging import (
    Configuration,
    ApiClient,
    MessagingApi,
    ReplyMessageRequest,
    TextMessage,
    MessagingApiBlob,
    ShowLoadingAnimationRequest,
)
from linebot.v3.webhooks import MessageEvent, UserSource


class LineBotServices:
    def __init__(self, line_token):
        self.configuration = Configuration(access_token=line_token)

    def reply_message(self, reply_token, text, quote_status=False, quote_token=""):
        if text is not None and text != "":
            with ApiClient(self.configuration) as api_client:
                line_bot_api = MessagingApi(api_client)
                line_bot_api.reply_message_with_http_info(
                    ReplyMessageRequest(
                        reply_token=reply_token,
                        messages=[
                            TextMessage(
                                text=text,
                                quote_token=quote_token if quote_status else None,
                            )
                        ],
                    )
                )
        else:
            return

    def get_message_content(self, id):
        with ApiClient(self.configuration) as api_client:
            line_bot_api = MessagingApiBlob(api_client)

        return line_bot_api.get_message_content_with_http_info(message_id=id)

    def get_message_content_transcoding(self, id):
        with ApiClient(self.configuration) as api_client:
            line_bot_api = MessagingApiBlob(api_client)

        return (
            line_bot_api.get_message_content_transcoding_by_message_id_with_http_info(
                message_id=id
            )
        )

    def check_status(self, event: MessageEvent):
        if not isinstance(event.source, UserSource):
            return False, "很抱歉，用戶資料錯誤"

        if event.mode == "standby":
            return False, "很抱歉，現在休息中"

        return True, ""

    def get_profile(self, user_id):
        with ApiClient(self.configuration) as api_client:
            line_bot_api = MessagingApi(api_client)

        return line_bot_api.get_profile_with_http_info(user_id=user_id)

    def show_loading(self, user_id, loadingSeconds=10):
        with ApiClient(self.configuration) as api_client:
            line_bot_api = MessagingApi(api_client)
            line_bot_api.show_loading_animation_with_http_info(
                ShowLoadingAnimationRequest(
                    chatId=user_id,
                    loadingSeconds=loadingSeconds,
                )
            )
