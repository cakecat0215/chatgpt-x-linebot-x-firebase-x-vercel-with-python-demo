import threading
from domain.message import Message
from domain.user import User
from domain.audio import Audio
from ai.line_bot.services import LineBotServices
from task.manager import TaskManager
from cache.user_cache import UserCache

user_cache = UserCache()
user_locks_lock = threading.Lock()


class EventManager:

    def __init__(
        self,
        line_bot_services: LineBotServices,
        message: Message,
    ):
        self.line_bot_services = line_bot_services
        self.task_manager = TaskManager()
        self.message = message

    async def start(self):
        if user_cache.check_dismiss_time(user_id=self.message.user_id):
            return

        if user_cache.check_request_time(user_id=self.message.user_id):
            self.line_bot_services.reply_message(
                reply_token=self.message.reply_token,
                text="重複發送太多訊息了，請待會再試一次！",
            )
            return

        user_cache.set_dismiss_time(user_id=self.message.user_id)
        with user_locks_lock:
            if not user_cache.check_lock(user_id=self.message.user_id):
                user_cache.set_lock(user_id=self.message.user_id)
            else:
                return
        user_lock = user_cache.get_lock(user_id=self.message.user_id)
        if user_lock.acquire(blocking=False):
            try:
                user = await self.task_manager.get_user_from_message(
                    message=self.message
                )
                if user.check_restricted() and user.user_id:
                    restricted_time = user.get_restricted_time()
                    reply_message = f"很抱歉，您的額度已使用完畢，請等待至以下時間 {restricted_time} 解鎖服務!"
                    self.line_bot_services.reply_message(
                        reply_token=self.message.reply_token, text=reply_message
                    )
                else:
                    await self.reply_event(user=user)

            except Exception as e:
                self.line_bot_services.reply_message(
                    reply_token=self.message.reply_token,
                    text=f"Error! {e}",
                )

            finally:
                user.upgrade_chat_time()
                await self.task_manager.update_user_data(user=user)
                user_cache.del_lock(user_id=self.message.user_id)
                with user_locks_lock:
                    user_lock.release()
        else:
            return

    async def text_event(self, user: User):
        reply_message = await self.task_manager.text_reply_task(
            message=self.message, user=user
        )
        self.line_bot_services.reply_message(
            reply_token=self.message.reply_token, text=reply_message
        )

    async def reply_event(self, user: User):
        if self.message.type == "text":
            await self.text_event(user=user)
        elif self.message.type == "audio":
            await self.audio_event(user=user)
        else:
            reply_message = "很抱歉，目前只接受文字或語音訊息，感謝您"
            self.line_bot_services.reply_message(
                reply_token=self.message.reply_token, text=reply_message
            )

    async def audio_event(self, user: User):
        try:
            reply_message = ""
            audio = Audio()
            recognize_language = audio.to_recognize_language(
                language=self.message.language
            )
            if recognize_language is None:
                reply_message = "您所在的地區語言不支持"
                return

            transcoding = self.line_bot_services.get_message_content_transcoding(
                id=self.message.message_id
            )
            transcoding_status_code = transcoding.status_code
            if transcoding_status_code != 200:
                reply_message = "還不能接收音訊來源"
                return

            audio_data = self.line_bot_services.get_message_content(
                id=self.message.message_id
            )
            audio_info = (audio_data.status_code, audio_data.data)
            if audio_info[0] != 200:
                reply_message = "音訊來源發生錯誤"
                return

            # 音訊轉檔方法，需自行替換，否則機器人會回傳'語音轉檔失敗!'訊息，本專案音訊轉檔方法採用額外建構的轉檔項目，不另行提供
            # 可參考網路上音訊轉wav檔方法，確保能成功得到wav檔即可
            wav_data = await audio.get_wav_data(audio_data=audio_info[1])
            if wav_data is None:
                reply_message = "語音轉檔失敗!"
                return

            reply_message = await self.task_manager.audio_reply_task(
                message=self.message,
                user=user,
                wav_data=wav_data,
                language=recognize_language,
            )

        finally:
            self.line_bot_services.reply_message(
                reply_token=self.message.reply_token, text=reply_message
            )
