import time
from datetime import datetime
from collections import deque
from config.env import env

tokens_limit = env["TOKENS_LIMIT"]
msg_list_limit = env["MSG_LIST_LIMIT"]


class User:
    def __init__(
        self,
        user_id,
        message_list=deque(maxlen=msg_list_limit),
        display_name="",
        first_chat_time=time.time(),
        last_chat_time=time.time(),
        tokens_use=0,
        restricted=False,
        restricted_time="None",
        ai_background="",
        ai_guidelines="",
    ):
        self.user_id = user_id
        self.display_name = display_name
        self.first_chat_time = first_chat_time
        self.last_chat_time = last_chat_time
        self.tokens_use = tokens_use
        self.restricted = restricted
        self.restricted_time = restricted_time
        self.message_list = message_list
        self.ai_guidelines = ai_guidelines
        self.ai_background = ai_background

    def upgrade_chat_time(self, last_chat_time=time.time()):
        self.last_chat_time = last_chat_time

    def set_restricted(self):
        hours_in_seconds = self.last_chat_time + 24 * 60 * 60
        self.restricted = True
        self.restricted_time = str(hours_in_seconds)

    def update_tokens_use(self, tokens):
        self.tokens_use += tokens
        if self.tokens_use >= tokens_limit:
            self.set_restricted()

    def un_restrict(self):
        self.restricted = False
        self.restricted_time = "None"
        self.tokens_use = 0

    def check_restricted(self) -> bool:
        if self.restricted:
            if time.time() >= float(self.restricted_time):
                self.un_restrict()
                return False
            else:
                return True

        return False

    def set_ai_background(self, ai_background: str):
        self.ai_background = ai_background

    def set_ai_guidelines(self, ai_guidelines: str):
        self.ai_guidelines = ai_guidelines

    def get_restricted_time(self):
        _restricted_time = float(self.restricted_time)
        _datetime = datetime.fromtimestamp(_restricted_time)
        formatted_time = _datetime.strftime("%Y-%m-%d %H:%M")
        return formatted_time

    def add_msg(
        self,
        new_msg,
        role="user",
    ):
        if len(self.message_list) > msg_list_limit:
            _list = self.message_list[:msg_list_limit]
            self.message_list = _list
        self.message_list.append({"role": role, "content": new_msg})

    def get_message_list(self):
        return list(self.message_list)

    def to_message_deque(self, data_list):
        if data_list is not None:
            self.message_list = deque(data_list, maxlen=msg_list_limit)

    def to_dict(self):
        return {
            "id": self.user_id,
            "name": self.display_name,
            "first_chat_time": self.first_chat_time,
            "last_chat_time": self.last_chat_time,
            "tokens_use": self.tokens_use,
            "restricted": self.restricted,
            "restricted_time": self.restricted_time,
            "message_list": self.get_message_list(),
            "ai_background": self.ai_background,
            "ai_guidelines": self.ai_guidelines,
        }

    def __str__(self):
        return f"User Id: {self.user_id}, User Display Name: {self.display_name}, First Chat Time: {self.first_chat_time}, Last Chat Time: {self.last_chat_time}, Tokens Use: {self.tokens_use}, Restricted: {self.restricted}, Restricted Time: {self.restricted_time}, Ai Guidelines: {self.ai_guidelines}, Ai Background: {self.ai_background}, Message List: {self.message_list}."
