
class Message():
    def __init__(
        self,
        reply_token,
        user_id,
        message_id,
        type,
        timestamp,
        quote_token="",
        display_name="",
        content=None,
        language="zh-TW"
    ):
        self.reply_token = reply_token
        self.user_id = user_id
        self.message_id = message_id
        self.display_name = display_name
        self.type = type
        self.content = content
        self.quote_token = quote_token
        self.timestamp = timestamp
        self.language = language

    def __str__(self):
        return f"Reply Token: {self.reply_token}, Quote Token: {self.quote_token}, User Id: {self.user_id}, Message Id: {self.message_id}, User Display Name: {self.display_name}, Type: {self.type}, Content: {self.content}, Timestamp: {self.timestamp}, Language: {self.language}."
