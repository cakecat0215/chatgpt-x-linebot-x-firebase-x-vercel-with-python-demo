import requests
import io
import base64
from config.env import env

recognize_language = {
    "zh-TW": "cmn-Hant-TW",
    "zh-CN": "cmn-Hans-CN",
    "ja": "ja-JP",
    "en": "en-US",
    "ko": "ko-KR",
}


class Audio:

    def to_recognize_language(self, language):
        return recognize_language.get(language)

    # 音訊轉檔方法，需自行替換，否則機器人會回傳'語音轉檔失敗!'訊息，本專案音訊轉檔方法採用額外建構的轉檔項目，不另行提供
    # 可參考網路上音訊轉wav檔方法，確保能成功得到wav檔即可
    async def get_wav_data(self, audio_data):
        wav_data = None
        return wav_data
