from domain.user import User
from domain.message import Message
from database.firebase import Firebase

firebase = Firebase()


class FirebaseServices:

    async def update_data(self, user: User, data):
        path = f"users/{user.user_id}"
        firebase.update_data(path=path, data=data)

    async def create_user_from_message(self, message: Message):
        path = f"users/{message.user_id}"
        user = User(
            user_id=message.user_id,
            first_chat_time=message.timestamp,
            display_name=message.display_name,
        )
        firebase.write_data(path=path, data=user.to_dict())

    async def get_user_from_message(self, message: Message) -> User:
        path = f"users/{message.user_id}"
        data = firebase.read_data(path=path)
        user = User(
            user_id=data.get("id"),
            display_name=data.get("name"),
            first_chat_time=data.get("first_chat_time"),
            last_chat_time=data.get("last_chat_time"),
            tokens_use=data.get("tokens_use"),
            restricted=data.get("restricted"),
            restricted_time=data.get("restricted_time"),
            ai_background=data.get("ai_background"),
            ai_guidelines=data.get("ai_guidelines"),
        )
        user.to_message_deque(data_list=data.get("message_list"))
        return user

    async def check_user_from_message(self, message: Message) -> bool:
        path = f"users/{message.user_id}"
        return firebase.data_exists(path=path)
