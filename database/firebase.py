from firebase_admin import credentials, db, initialize_app
from config.env import env, file_dict

assert isinstance(
    env["FIREBASE_URL"], str
), "Please set the environment variables Firebase Settings."


class Firebase:
    def __init__(self):
        self.cred = credentials.Certificate(file_dict)
        self.default_app = initialize_app(
            self.cred, {"databaseURL": env["FIREBASE_URL"]}
        )
        self.db_ref = db.reference()

    def write_data(self, path, data):
        ref = self.db_ref.child(path)
        ref.set(data)

    def read_data(self, path):
        ref = self.db_ref.child(path)
        return ref.get()

    def update_data(self, path, data):
        ref = self.db_ref.child(path)
        ref.update(data)

    def delete_data(self, path):
        ref = self.db_ref.child(path)
        ref.delete()

    def data_exists(self, path):
        ref = self.db_ref.child(path)
        return ref.get() is not None
