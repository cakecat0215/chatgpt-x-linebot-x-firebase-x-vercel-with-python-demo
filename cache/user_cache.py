import time
import threading
from cachetools import TTLCache
from config.env import env

assert isinstance(
    env["REPEATED_TIME"], float
), "Please set the environment variables REPEATED TIME Settings."

assert isinstance(
    env["DISMISS_TIME"], float
), "Please set the environment variables DISMISS TIME Settings."


class UserCache:

    def __init__(self):
        self.request_time_data = TTLCache(maxsize=1000, ttl=env["REPEATED_TIME"])
        self.dismiss_time_data = TTLCache(maxsize=1000, ttl=env["DISMISS_TIME"])
        self.lock_data = TTLCache(maxsize=1000, ttl=10)
        self.state_data = TTLCache(maxsize=2000, ttl=600)

    def set_lock(self, user_id, request_time: float = time.time()):
        self.lock_data[user_id] = threading.Lock()
        self.request_time_data[user_id] = request_time

    def set_dismiss_time(self, user_id):
        self.dismiss_time_data[user_id] = user_id

    def check_lock(self, user_id):
        return self.lock_data.get(user_id) is not None

    def check_request_time(self, user_id):
        return self.request_time_data.get(user_id) is not None

    def check_dismiss_time(self, user_id):
        return self.dismiss_time_data.get(user_id) is not None

    def del_lock(self, user_id):
        if user_id in self.lock_data:
            del self.lock_data[user_id]

    def get_lock(self, user_id):
        return self.lock_data[user_id]

