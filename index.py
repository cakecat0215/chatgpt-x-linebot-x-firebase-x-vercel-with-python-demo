from flask import Flask
from ai.line_bot.bot import LineBot

app = Flask(__name__)
line_bot = LineBot()


@app.route("/")
def hello():
    return "Hello :) !"


@app.route("/chatgpt", methods=["POST"])
def callback():
    return line_bot.line_handler()


if __name__ == "__main__":
    app.run()
