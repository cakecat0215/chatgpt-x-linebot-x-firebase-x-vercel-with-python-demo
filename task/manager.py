from database.services import FirebaseServices
from domain.message import Message
from domain.user import User
from factory.data_factory import DataFactory
from utils.tool import Tool


class TaskManager:

    def __init__(self):
        self.firebase_services = FirebaseServices()

    async def update_user_data(self, user: User):
        await self.firebase_services.update_data(user=user, data=user.to_dict())

    async def get_user_from_message(self, message: Message) -> User:
        check_user = await self.firebase_services.check_user_from_message(
            message=message
        )
        if not check_user:
            await self.firebase_services.create_user_from_message(message=message)
        user: User = await self.firebase_services.get_user_from_message(message=message)
        await self.update_user_data(user=user)
        return user

    async def text_reply_task(self, message: Message, user: User) -> str:
        user_use_tokens = Tool.num_tokens_from_string(string=message.content)
        if user_use_tokens > 105:
            reply_message = "請表達約50字以內的內容"
            return reply_message

        (
            reply_message,
            total_use_tokens,
        ) = await DataFactory().reply_message_with_gpt(message=message, user=user)
        user.upgrade_chat_time()
        user.update_tokens_use(tokens=total_use_tokens)
        await self.update_user_data(user=user)
        return reply_message

    async def audio_reply_task(
        self, message: Message, user: User, wav_data, language
    ) -> str:
        _reply_msg = ""
        _reply_msg = await DataFactory().recognize_speech(
            wav_data=wav_data, language=language
        )
        message.content = _reply_msg
        _reply_msg = await self.text_reply_task(message=message, user=user)
        return _reply_msg
