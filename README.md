# ChatGPT x LineBot x Firebase x Vercel with Python Demo 

### 實現24小時聊天AI 條件式支持語音輸入功能
> 更新記錄:  

> 20240826 優化代碼邏輯 調整項目結構 修復Bug 條件式支持語音輸入功能

> 20240616 新增說明  

> 20240618 新增用戶緩存數據 更新共3項環境變數 優化回覆邏輯 bug fix 套件更新  



## 效果展示

![](images/語音輸入功能.mp4)

![圖片](images/展示圖片1.jpg)

![](images/展示影片1.mp4)

![](images/展示影片2.mp4)

## 展示用公眾號QRCode(專長日文翻譯的聊天AI 顯示日文以及中文語言訊息)

![圖片](images/QRCode.jpg)

## 特別說明

### 因語音轉wav檔功能需要ffmpeg，放入專案後打包大小過大，無法部署在Vercel(本專案採用免費計畫)

### 語音轉wav檔功能需自行逕行解決，本專案解決方式為額外建構轉檔項目逕行解決

### 若能解決音訊轉檔取得wav檔，那本專案是支持語音輸入功能

### 在 [這個](domain/audio.py) 文件中，以下方法需自行替換可轉wav檔方法，否則不支持語音輸入功能：

```python

    # 音訊轉檔方法，需自行替換，否則機器人會回傳'語音轉檔失敗!'訊息，本專案音訊轉檔方法採用額外建構的轉檔項目，不另行提供
    # 可參考網路上音訊轉wav檔方法，確保能成功得到wav檔即可
    async def get_wav_data(self, audio_data):
        wav_data = None
        return wav_data
```


## 安裝

* git clone到本地端目錄

* 建議使用 Poetry 套件管控 ( 亦是本專案採用方式 ) 詳情教學參考：[Python 套件管理器——Poetry 完全入門指南](https://blog.kyomind.tw/python-poetry/) 感謝大佬分享

* 開啟虛擬機指令

`poetry shell`

* 安裝指令

`poetry install`

* 鎖指令

`poetry lock`

## 套件更新(如有需要)

* 更新指令

`poetry update`

### 更多更新指令可參照教學參考：[Python 套件管理器——Poetry 完全入門指南](https://blog.kyomind.tw/python-poetry/)

## 退出虛擬機(如有需要)

* 退出指令

`exit`

## 前置作業

### 取得LINE CHANNEL ACCESS TOKEN 和 LINE CHANNEL SECRET

* [Line 開發者平台網址](https://developers.line.biz/en/)

* 創立公眾號，取得並將LINE CHANNEL ACCESS TOKEN 和 LINE CHANNEL SECRET 複製 然後貼在 [這裡](.env) 找到對應Key貼在等號右側

### 取得OPENAI API KEY

* [Open Ai Api 後台](https://openai.com/index/openai-api/)

* 創建一個新的Secret key 並將其貼上 [這裡](.env) 找到對應Key貼在等號右側

### 取得FIREBASE SERVICE ACCOUNT KEY 和 FIREBASE Realtime Database URL

* [FIREBASE 後台](https://firebase.google.com/?hl=zh-cn)

* 創建項目後 啟用 Realtime Database 數據庫 記得設置規則 rules 完成後複製參考網址 並將其貼上 [這裡](.env) 找到對應Key貼在等號右側 

### FIREBASE SERVICE ACCOUNT KEY 取得方式

* 點擊左上角 項目設置

![圖片](images/Firebase項目設置.jpg)

* 選擇 服務帳號 -> 生成新私鑰

![圖片](images/生成新私鑰.jpg)

* 下載目錄下找到 your-project-name.json 就是 SERVICE ACCOUNT KEY

* 開啟 your-project-name.json 將每個key值對應的value貼上 [這裡](.env) 找到對應Key貼在等號右側

### 其他參數設定

* 到這裡主要Key值都設定好 其他參數剩餘

```python
#ChatGPT Settings
CHATGPT_MODEL=""  #open ai 聊天模型 公眾號預設是採用 gpt-4o-mini 模型
CHATGPT_TEMPERATURE="" #open ai 溫度 數字越大回覆內容越廣 預設0.1

#Chat Settings
DISMISS_TIME=""  #拒絕回應時間 單位秒 預設3秒
REPEATED_TIME=""  #發送重複訊息回應 單位秒 預設6秒
TOKENS_LIMIT=""  #用戶額度限制 採 token 數量計算 超過設置 token 數量會限制發言，24小時後重置數量 預設 10000 token 數量

#Prompt Settings
MSG_LIST_LIMIT="" #open ai 聊天追朔參考紀錄行數 數字越大參考的數據越多 使用的token數量也會越多
AI_GUIDELINES=""  #機器人聊天風格設定 預設""
AI_BACKGROUND=""  #機器人背景設定 預設""
```

* 這些參數都有預設值

## 執行(請先進入虛擬機)

* 執行指令

`python .\index.py`

* 開新的終端機

* 代理指令

`ngrok http 5000`

* 複製代理後的公開網址 回到 Line 開發者平台 在 Webhook URL 欄位上貼上網址 後面記得補上`/chatgpt`

![圖片](images/Webhook%20URL.jpg)

* 完成後點擊verify確認 回傳Success就是成功 接下來將自己創建的Line公眾號添加好友 然後進行聊天

## 部署雲端服務器

#### 注意!! 切勿上傳 [環境參數檔案](.env) 以及 SERVICE ACCOUNT KEY !! 

#### 注意!! 切勿上傳 [環境參數檔案](.env) 以及 SERVICE ACCOUNT KEY !!

#### 注意!! 切勿上傳 [環境參數檔案](.env) 以及 SERVICE ACCOUNT KEY !!

* 將專案推送至遠端倉庫 github gitlab 或者主流的倉庫都可

* [Vercel 網址](https://vercel.com/home)

* 註冊完後會顯示import選項 選擇遠端倉庫的專案 預設環境變數 Environment Variables 先不用設定 直接部屬

* 部署完成後項目選單列裡的 Settings

![圖片](images/Settings.jpg)

* 選擇 Environment Variables

![圖片](images/Environment%20Variables.jpg)

* 右側下拉 使用導入 .env 設定 Environment Variables

![圖片](images/Import.env.jpg)

* 完成後重新再部署一次 執行 Redeploy

![圖片](images/Redeploy.jpg)

* 部屬完成後 複製Domains網址 最上方的網址

![圖片](images/Domains.jpg)

* 回到 Line 開發者平台 將 Domain 網址 取代掉之前的 代理網址 後面一樣補上`/chatgpt`

* 完成後點擊verify確認 回傳Success就是成功 24小時聊天AI完成


### 此專案對應FIREBASE Realtime Database結構為

```python
根目錄/
│
└───users/
    ├───user_id_1/
    │   └───{user_data}
    │
    ├───user_id_2/
    │   └───{user_data}
    │
    ├───user_id_3/
    │   └───{user_data}
    │
    └───...
```

### user data 設定可參照 [這裡](domain/user.py)



