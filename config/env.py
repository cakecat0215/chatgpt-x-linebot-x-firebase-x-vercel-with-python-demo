import os


def _env():
    return {
        "LINE_TOKEN": os.getenv("LINE_TOKEN"),
        "LINE_SECRET": os.getenv("LINE_SECRET"),
        "OPENAI_API_KEY": os.getenv("OPENAI_API_KEY"),
        "CHATGPT_MODEL": os.getenv("CHATGPT_MODEL", default="gpt-4o-mini"),
        "CHATGPT_TEMPERATURE": float(os.getenv("CHATGPT_TEMPERATURE", default=0.1)),
        "MSG_LIST_LIMIT": int(os.getenv("MSG_LIST_LIMIT", default=5)),
        "AI_GUIDELINES": os.getenv("AI_GUIDELINES", default=""),
        "AI_BACKGROUND": os.getenv("AI_BACKGROUND", default=""),
        "REPEATED_TIME": float(os.getenv("REPEATED_TIME")),
        "DISMISS_TIME": float(os.getenv("DISMISS_TIME")),
        "FIREBASE_URL": os.getenv("FIREBASE_URL"),
        "TOKENS_LIMIT": int(os.getenv("TOKENS_LIMIT", default=10000)),
    }


def create_keyfile_dict():
    return {
        "type": os.getenv("TYPE"),
        "project_id": os.getenv("PROJECT_ID"),
        "private_key_id": os.getenv("PRIVATE_KEY_ID"),
        "private_key": os.getenv("PRIVATE_KEY"),
        "client_email": os.getenv("CLIENT_EMAIL"),
        "client_id": os.getenv("CLIENT_ID"),
        "auth_uri": os.getenv("AUTH_URI"),
        "token_uri": os.getenv("TOKEN_URI"),
        "auth_provider_x509_cert_url": os.getenv("AUTH_PROVIDER_X509_CERT_URL"),
        "client_x509_cert_url": os.getenv("CLIENT_X509_CERT_URL"),
        "universe_domain": os.getenv("SCOPE"),
    }


file_dict = create_keyfile_dict()
env = _env()
