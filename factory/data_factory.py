from speech_recognition import Recognizer, AudioFile
from ai.open_ai.bot import ChatGPT
from domain.message import Message
from domain.user import User
from ai.open_ai.prompt import Prompt
from utils.tool import Tool

chatgpt = ChatGPT()


class DataFactory:

    async def reply_message_with_gpt(self, message: Message, user: User):
        reply_message = ""
        user.add_msg(new_msg=f"{message.display_name}:{message.content}?")
        to_ai_msg = (
            Prompt.add_ai_background_msg(user=user)
            + user.get_message_list()
            + Prompt.add_ai_guidelines_msg(user=user)
        )
        reply_message = chatgpt.get_response(message_list=to_ai_msg).replace(
            "AI:", "", 1
        )
        gpt_total_use_tokens = chatgpt.total_tokens
        msg_total_use_tokens = Tool.num_tokens_from_list(msg_list=to_ai_msg)
        user.add_msg(
            new_msg=f"AI:{reply_message}",
            role="assistant",
        )
        return (
            reply_message,
            gpt_total_use_tokens + msg_total_use_tokens,
        )

    async def recognize_speech(self, wav_data, language="zh-TW"):
        speech = ""
        r = Recognizer()
        audio_ex = AudioFile(wav_data)
        with audio_ex as source:
            audio_data = r.record(source=audio_ex)
            speech = r.recognize_google(
                audio_data=audio_data,
                language=language,
            )
        return speech
