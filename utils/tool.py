import tiktoken


class Tool:

    @staticmethod
    def num_tokens_from_string(string: str) -> int:
        encoding = tiktoken.get_encoding("cl100k_base")
        num_tokens = len(encoding.encode(string))
        return num_tokens

    @staticmethod
    def num_tokens_from_list(msg_list: list) -> int:
        if not msg_list:
            return 0

        encoding = tiktoken.get_encoding("cl100k_base")
        content_list = [item["content"] for item in msg_list]
        token_counts = [len(encoding.encode(text)) for text in content_list]
        total_tokens = sum(token_counts)
        return total_tokens
